import pandas as pd

def calculate_average_humidity(input_file):
    """
    Calculates the average relative humidity from a CSV file using pandas.

    :param input_file: The path to the input CSV file
    :return: The average relative humidity, or 0 if no valid entries are found
    """
    try:
        # Read the CSV file into a pandas DataFrame
        weather_data_rows = pd.read_csv(input_file)
        # Filter rows where the parameter name is "RH_PT1H_AVG" and value is not NaN
        rh_rows = weather_data_rows[(weather_data_rows['ParameterName'] == "RH_PT1H_AVG") & (weather_data_rows['ParameterValue'] != "NaN")]
        # Convert ParameterValue to numeric, coercing errors to NaN, and drop NaN values
        rh_rows = rh_rows.copy()
        rh_rows['ParameterValue'] = pd.to_numeric(rh_rows['ParameterValue'], errors='coerce')
        rh_rows = rh_rows.dropna(subset=['ParameterValue'])
        # Calculate the average
        if rh_rows.empty:
            return 0
        return rh_rows['ParameterValue'].mean()
    except FileNotFoundError:
        print(f"Error: File not found - {input_file}")
        return 0
    except Exception as e:
        print(f"Error processing the file: {e}")
        return 0


input_file = "weather_2020-02.csv"
average_humidity = calculate_average_humidity(input_file)
print(f"Average Relative Humidity: {average_humidity:.1f}")